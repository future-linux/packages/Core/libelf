# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=(debuginfod elfutils libelf)
pkgbase=elfutils
pkgver=0.187
pkgrel=2
pkgdesc="Handle ELF object files and DWARF debugging information"
arch=('x86_64')
url="https://sourceware.org/elfutils/"
license=('LGPL3' 'GPL3')
makedepends=('bzip2' 'curl' 'gcc-libs' 'libarchive'
'libmicrohttpd' 'sqlite' 'xz' 'zlib' 'zstd')
options=('staticlibs')
source=(https://sourceware.org/ftp/elfutils/${pkgver}/${pkgbase}-${pkgver}.tar.bz2)
sha256sums=(e70b0dfbe610f90c4d1fe0d71af142a4e25c3c4ef9ebab8d2d72b65159d454c8)

prepare() {
    (
        cd ${pkgbase}-${pkgver}

        autoreconf -fiv
    )

    cp -av ${pkgbase}-${pkgver} ${pkgbase}-test-${pkgver}

}

build() {
    (
        # fat-lto-objects is required for non-mangled .a files in libelf
        CFLAGS+=" -ffat-lto-objects"

        cd ${pkgbase}-${pkgver}

        ${configure}               \
            --sysconfdir=/etc      \
            --program-prefix="eu-" \
            --enable-deterministic-archives

        make
    )

    (
        cd ${pkgbase}-test-${pkgver}

        # debugging information is required for test-suite
        CFLAGS+=" -g"
        # fat-lto-objects is required for non-mangled .a files in libelf
        CFLAGS+=" -ffat-lto-objects"

        ${configure}               \
            --sysconfdir=/etc      \
            --program-prefix="eu-" \
            --enable-deterministic-archives
        make
    )
}

package_debuginfod() {
    pkgdesc+=" (debuginfod)"
    depends=('gcc-libs' 'glibc' 'libarchive' "libelf=${pkgver}"
        'libmicrohttpd' 'sqlite')

    make DESTDIR=${pkgdir} install -C ${pkgbase}-${pkgver}

    install -vDm 644 ${pkgbase}-${pkgver}/{AUTHORS,ChangeLog,NEWS,NOTES,README} -t ${pkgdir}/usr/share/doc/$pkgname

    # set the default DEBUGINFOD_URLS environment variable to the distribution's debuginfod URL
    printf "https://debuginfod.archlinux.org\n" > ${pkgdir}/etc/debuginfod/archlinux.urls

    (
        cd ${pkgdir}

        _pick libelf usr/{include,lib64}
        _pick elfutils usr/bin/eu-*
        _pick elfutils usr/share/locale
        _pick elfutils usr/share/man/man1/eu-*
        _pick elfutils usr/share/man/man3/elf_*
    )

}

package_elfutils() {
  pkgdesc+=" (utilities)"
  depends=('gcc-libs' 'glibc' "libelf=${pkgver}")

    mv -v elfutils/* ${pkgdir}

    install -vDm 644 $pkgbase-$pkgver/{AUTHORS,ChangeLog,NEWS,NOTES,README} -t ${pkgdir}/usr/share/doc/${pkgname}/
}

package_libelf() {
    pkgdesc+=" (libraries)"
    depends=('gcc-libs' 'bzip2' 'curl' 'xz' 'zlib' 'zstd')

    # NOTE: the shared objects can not be added to provides as they are not versioned

    mv -v libelf/* ${pkgdir}
    install -vDm 644 ${pkgbase}-${pkgver}/{AUTHORS,ChangeLog,NEWS,NOTES,README} -t ${pkgdir}/usr/share/doc/${pkgname}/
}
